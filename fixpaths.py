#!/usr/bin/env python3
import sqlite3
import os
from argparse import ArgumentParser


def get_args():
    parser = ArgumentParser()
    parser.add_argument('coveragefile', metavar='FILENAME', help='Path to the .coverage file')
    return parser.parse_args()


def fix_paths(coverage_file):
    # Get the file name, and extrapolate the base path
    file_name = os.path.abspath(coverage_file)
    base_path = os.path.dirname(file_name)
    # Open a connection to the SQLite database
    db = sqlite3.connect(file_name)
    cursor = db.cursor()
    # Get all the files in the database and update their filenames
    cursor.execute('SELECT id, path FROM file')
    new_files = []
    for row in cursor.fetchall():
        new_files.append((os.path.join(base_path, row[1].split('openlp/openlp/')[-1]), row[0]))
    cursor.executemany('UPDATE file SET path = ? WHERE id = ?', new_files)
    db.commit()
    cursor.close()
    db.close()


if __name__ == '__main__':
    args = get_args()
    fix_paths(args.coveragefile)
